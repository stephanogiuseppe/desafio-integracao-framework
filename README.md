# Proposta Histórico Fatura

Primeiramente obrigado por estar aqui! E querer trabalhar no Itaú Unibanco!.

Esse é nosso desafio 2 de 3, onde iremos avaliar sua habilidade com frameworks Javascript e requisições http.  
Para isso você irá consumir uma API onde estão os lançamentos de um cartão de credito dentro de um mesmo ano. Instruções de consumo da API [**neste link**](https://gitlab.com/desafio3/readme-api).

O método de avaliação é simples, tendo seu código em mãos iremos rodar o projeto e validar se você construiu o projeto atendendo ao cenário proposto e os requisitos.

Boa sorte!

### Cenário Proposto

_Neste projeto temos uma API que retorna os lançamentos de um cartão de forma desorganizada._  
_A necessidade do nosso projeto é criar o front, usando seu framework de preferência. A página criada pode ser similar a do desafio 1, deve conter uma lista de gastos gerais e uma lista de gastos consolidados por mês._

### Requisitos

* O framework utilizado deve ser um framework de Javascript.
* No html deve existir uma tabela **Mês|Total Gasto** e uma tabela geral com as colunas **Origem|Categoria|Valor Gasto|Mês**.
* Os campos **Mês** e **Categoria** em ambas as tabelas devem ser exibidos escrito.
* Os dados podem ser apresentados em formato de tabela na mesma página html ou em outra paágina dêsde que seja possível navegar para ela, pela página principal.
* Só devem ser exibidos na tabela de gastos por mês os meses em que houveram lançamentos.
* Essa tarefa deve ser entregue em um repositório público no Gitlab, o link desse repositório deve ser enviado para Felipe Marques Melo felipe.melo@itau-unibanco.com.br com seu nome completo e o link do git.


### Dica

* Usar um framework de CSS (como bootstrap) é recomendado mas não é obrigatório.
* Atente-se a organização do projeto isso será levado em conta.

**ATENÇÃO:** _Após o envio do repositório para o e-mail descrito nos requisitos não serão aceitos novos commits, essa atividade deve ser enviada até 06/04/2020 10:00._ 

### Bom Desempenho!

### #QueremosVoceNoItau

## PRÓXIMO PASSO: [DESAFIO 3 / 3](https://gitlab.com/desafio3/desafio-final)